module.exports = {
    theme: {
        theme: {
            customForms: theme => ({
              default: {
                checkbox: {
                  width: theme('spacing.6'),
                  height: theme('spacing.6'),
                  iconColor: theme('colors.red.700'),
                },
              },
            })
          },
     
    purge: [
        './pages/**/*.tsx',
        './components/**/*.tsx'
    ],
    plugins: [
        require('@tailwindcss/custom-forms'),
      ]
    
  }}