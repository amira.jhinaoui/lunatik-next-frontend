import styles from './HeaderApp.module.css'
import { SideMenu } from './SideMenu'
export function HeaderApp(){
    return(
        <>
        <nav className="flex items-center justify-between flex-wrap bg-gray-800 p-6">
  <div className="flex items-center flex-shrink-0 text-white mr-6">
  <img
              src="/images/logo.png"
              className={`${styles.headerHomeImage} `}
              alt="logo"
 />
 </div>
     <div className=" flex flex-row-reverse font-semibold-serif text-l tracking-tight  m-4">
     <div className="mx-12">
     <img
              src="/images/avatar.png"
              className={`${styles.AvatarImage} `}
              alt="logo"
 />
 </div>
     <input className="  bg-gray-700 rounded " type="text" placeholder="Search..."/>
     
     </div>
 

</nav>

     </>
    )
}