
import { HeaderApp } from "./HeaderApp";
import { SideMenu } from "./SideMenu";
import {Layout} from 'antd';

export  function LayoutApp ({children}:any) {
    const { Header, Sider, Content } = Layout;
    return(
        <>
        <Layout>
            <Header> <HeaderApp/> </Header>   

            <Content> {children}</Content>
           
        </Layout>
      </> 
    );
}