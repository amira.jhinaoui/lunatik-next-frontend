import { Action } from 'redux';
import { IFailure } from '../../utils/types';

export interface IUser{
    id: string;
    firstName: string;
    lastname: string;
    birthday: Date;
    gender:string;
    email:string;
    phone:string;
    phoneExtra?:string;
    enabled:boolean;
    zipCode:string;
    country:string;
    createdAt?:Date;
    updatedAt?:Date;
    avatar:string;
    role:string;
    city:string;
    adress:string;
}
export interface IUsers{
    listUsers:IUser[];
}

export const GET_USERS='GET_USERS';
export type GET_USERS= typeof GET_USERS;

export const GET_USERS_SUCCESS='GET_USERS_SUCCESS';
export type GET_USERS_SUCCESS = typeof GET_USERS_SUCCESS;

export const GET_USERS_FAILURE = 'GET_USERS_FAILURE';
export type GET_USERS_FAILURE = typeof GET_USERS_FAILURE;

export const GET_USER ='GET_USER';
export type GET_USER= typeof GET_USER;

export const GET_USER_SUCCESS='GET_USER_SUCCESS';
export type GET_USER_SUCCESS= typeof GET_USER_SUCCESS;

export const GET_USER_FAILURE= 'GET_USER_FAILURE';
export type GET_USER_FAILURE= typeof GET_USER_FAILURE;


export const ADD_USER='ADD_USER';
export type ADD_USER= typeof ADD_USER;

export const ADD_USER_SUCCESS= 'ADD_USER_SUCCESS';
export type ADD_USER_SUCCESS = typeof ADD_USER_SUCCESS;

export const ADD_USER_FAILURE ='ADD_USER_FAILURE';
export type ADD_USER_FAILURE= typeof ADD_USER_FAILURE;

export const UPDATE_USER ='UPDATE_USER';
export type UPDATE_USER= typeof UPDATE_USER;

export const UPDATE_USER_SUCCESS= 'UPDATE_USER_SUCCESS';
export type UPDATE_USER_SUCCESS= typeof UPDATE_USER_SUCCESS;

export const UPDATE_USER_FAILURE='UPDATE_USER_FAILURE';
export type UPDATE_USER_FAILURE = typeof UPDATE_USER_FAILURE;

export interface IGetUsers extends Action <GET_USERS> {
    payload: IUsers;
}

export interface IGetUsersSucess extends Action <GET_USERS_SUCCESS> {}
export interface IGetUsersFailure extends Action <GET_USERS_FAILURE> {
    payload: IFailure;
}

export interface IGetUser extends Action <GET_USER> {
    payload: IUser;
}
export interface IGetUserSuccess extends Action <GET_USERS_SUCCESS>{}
export interface IGetUserFailure extends Action <GET_USER_FAILURE> {
    payload: IFailure;
}
export interface IAddUser extends Action <ADD_USER> {
    payload: IUser;
}
export interface IAddUserSuccess extends Action <ADD_USER_SUCCESS>{}
export interface IAddUserFailure extends Action <ADD_USER_FAILURE> {
    payload: IFailure;
}
export type getUsersActionTypes =
  IGetUsers|
  IGetUser|
  IAddUser
