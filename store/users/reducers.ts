import { IUsers, getUsersActionTypes, GET_USERS, GET_USER, ADD_USER } from "./types";
import { Reducer } from "redux";

export const initialState:IUsers= {
    listUsers: []
}
export const usersReducer: Reducer<typeof initialState, getUsersActionTypes> = (state = initialState, action) => {
    switch (action.type) {
  
      case GET_USERS:
        return { ...state };
        case GET_USER:
          return {... state};
          case ADD_USER:
            return {...state};
      default:
          return state;
    }
};