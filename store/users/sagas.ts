import { takeLatest } from "redux-saga/effects";
import { apiResolver } from "next/dist/next-server/server/api-utils";


export function* getAllUsers(){
   yield apiResolver.call("http://api.lunatik.projets-en-cours.net/users")
}

export function* watchGetAllUsers() {
    // yield takeEvery(GET_USERS, getUsers);
  }
  

export function* getUser(){
   yield apiResolver.call("")
}

export function* watchGetUser() {
   
}

export function* addUser(){
   yield apiResolver.call("Api add new user")
}

export function watchAddUser (){
   
}