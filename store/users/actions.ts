import { ActionCreator } from "redux";
import { IGetUsers, IUsers, GET_USERS, IGetUser, GET_USER, IUser } from "./types";

export const getAllUsers: ActionCreator<IGetUsers> =(listUsers: IUsers)=>({
    type:GET_USERS,
    payload: listUsers
});

export const getUser: ActionCreator <IGetUser> =(user:IUser)=>({
    type: GET_USER,
    payload: user
})