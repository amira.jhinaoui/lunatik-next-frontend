import { usersReducer } from "./users/reducers";
import { Middleware, combineReducers, createStore ,applyMiddleware} from "redux";
import { watchGetAllUsers, watchGetUser, watchAddUser } from "./users/sagas";
import { all, fork } from 'redux-saga/effects';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

// Create a root reducer with combined reducers
const rootReducer = combineReducers({
  listUsers: usersReducer,
});



// Define GlobalState Type
export type GlobalState = ReturnType<typeof rootReducer>;



function* rootSaga() {
  yield all([
    fork(watchGetAllUsers),
    fork (watchGetUser),
    fork (watchAddUser),
  ]);
}



const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware();
  const middleware: Middleware[] = [sagaMiddleware];
  const middlewareEnhancer = applyMiddleware(...middleware);
  const composeEnhancers = composeWithDevTools({});

  const store = createStore(rootReducer, composeEnhancers(middlewareEnhancer));
  sagaMiddleware.run(rootSaga);

 
  return store;
};

const configurStor= ()=> {
  const sagaMiddelware= createSagaMiddleware ();
  const middlware :Middleware[]= [sagaMiddelware]; const middlewareEnhancer= applyMiddleware (... middlware);

}
export default configureStore;
