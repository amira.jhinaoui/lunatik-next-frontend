import {LayoutApp} from '../components/Layout'
import { TeamTable } from '../components/TeamTable'
export default function TeamList(){
    return (
    <LayoutApp >
      <TeamTable/>
    </LayoutApp>
    )
}