import initMiddleware from "../../lib/init-middleware";
import Cors from 'cors';


const cors= initMiddleware(
    //  here i can add my options
    Cors({
        methods:['GET','POST', 'OPTIONS'],
        RequestHeaders:['Authorization']
    })
)

export default async function handler(req,res){
    // i run cors
    await cors(req,res)
    res.json({message:"hello Api"})
}